import * as request from 'request-promise';
import * as cheerio from 'cheerio';
import * as validUrl from 'valid-url';
export class LinkProcessor {
    constructor() { }

    /**
     * Processes the url that is sent
     * @param req request object
     * @param res response object
     */
    public processLink(req, res): Promise<any> {
        let url = req.body.url || false;
        let _csrf = req.body._csrf || false;

        return new Promise<any>((resolve, reject) => {
            if (url && _csrf) {
                // Sets the header options
                let options = {
                    url: url,
                    gzip: true,
                    resolveWithFullResponse: true,
                    headers: {
                        'Accept': '*/*',
                        'Accept-Encoding': 'gzip, deflate',
                        'Accept-Language': 'en-US,en;q=0.8,pt;q=0.6'
                    }
                }
                // validate url
                if (validUrl.isUri) {
                    request(options).then((websiteData) => {
                        if (!websiteData.error) {
                            let websiteAnswers = this.getAnswers(websiteData.body);
                            websiteAnswers.siteProtocol = this.getSiteProtocol(websiteData);

                            this.renderView(res, _csrf, websiteAnswers);
                            resolve(websiteData);
                        } else {
                            this.renderView(res, _csrf, { error: websiteData });
                            resolve(websiteData);
                        }
                    }).catch((error) => reject(error));
                } else {
                    this.renderView(res, _csrf, { error: 'Bad url' });
                    resolve({ error: 'Bad url' });
                }
            } else {

                this.renderView(res, _csrf, { error: 'No url supplied or csrf supplied' });
                resolve({ error: 'No url supplied or csrf supplied' });
            }
        });
    }

    /**
     * Renders the specified view on
     * @param req request object
     * @param res response object
     */
    private renderView(res:any, _csrf: string, websiteAnswers?: any) {
        let options = (websiteAnswers) ? { message: 'Sputnik Technical Test', title: 'Sputnik Tecnical Test', _csrf, websiteAnswers } : { message: 'Sputnik Technical Test', title: 'Sputnik Tecnical Test', _csrf };
        res.render('index', options);
    }

    /**
     * Returns the title of the website
     * @param body response data from url
     * @returns {string} title of website
     */
    private getWebsiteTitle(body: string): string {
        let $: any = cheerio.load(body);
        let title: string = $('title').text();
        return title;
    }

    /**
     * Returns the number of links on the website
     * @param body response data from url
     * @returns {number} numberOfLinks
     */
    private getNumberOfLinks(body: string): number {
        let $: any = cheerio.load(body);
        let numberOfLinks: number = $('a').length;
        return numberOfLinks;
    }

    /**
     * Returns the number of unique links on the website
     * @param body response data from url
     * @returns {number} uniqueLinks
     */
    private getNumberOfUniqueDomains(body: string) {
        let $: any = cheerio.load(body);
        let uniqueArray = [];
        $('a').each((index, element) => {
            let link = $(element).attr('href');
            if (index === 0 && uniqueArray.length === 0) {
                uniqueArray.push(link);
            } else {
                let exist = uniqueArray.filter((href) => link === href);
                if (!exist.length) {
                    uniqueArray.push(exist);
                }
            }
        });
        return uniqueArray.length;
    }

    /**
     * Returns boolean
     * @param body HTML & CSS string
     * @returns {boolean} secure or not.True if secure.
     */
    public getSiteProtocol(websiteData: any): boolean {
        return websiteData.request.uri.protocol === 'https:';
    }

    /**
     * Returns boolean
     * @param body HTML 
     * @returns {boolean} Uses GA or not.
     */
    private usesGoogleAnalytics(body: string): boolean {
        let $: any = cheerio.load(body);
        let usesGA: boolean = false;
        let src: any = $('script');
        if (src) {
            Object.keys(src).forEach((key) => {
                if (src[key].attribs &&  src[key].attribs['src'] ) {
                    if (src[key].attribs['src'].indexOf('https://www.googletagmanager.com') !== -1 || src[key].attribs['src'].indexOf('google-analytics') !== -1) {
                        usesGA = true;
                    }   
                }
            });
        }
        return usesGA;
    }
    
    /**
     * Returns an object with the with
     * webpage title, number of anchors, number of srcs
     * @param body body of request. (Website HTML & Scripts)
     */
    public getAnswers(body: string): any {
        type Answers = {
            title: string;
            allLinks: number;
            uniqueDomains: number;
            siteProtocol: boolean;
            usesGA: boolean;
        };

        let userAnswers: Answers = {
            title: '',
            allLinks: 0,
            uniqueDomains: 0,
            siteProtocol: false,
            usesGA: false
        };
        
        userAnswers.title = this.getWebsiteTitle(body);
        userAnswers.allLinks = this.getNumberOfLinks(body);
        userAnswers.uniqueDomains = this.getNumberOfUniqueDomains(body);
        userAnswers.usesGA = this.usesGoogleAnalytics(body);
        return userAnswers;
    }
}