import { LinkProcessor } from '../controllers/linkProcessor';
import { expect } from 'chai';
import 'mocha';
import { link } from 'fs';


describe('Test Questions', () => {
    let linkProcessor = new LinkProcessor();
    let protocolData = { request: { uri: {protocol: 'http:'}}};
    let websiteData = '<!DOCTYPE html>\n<html lang="en">\n\t<head>\n\t<meta name="generator" content="Hugo 0.41-DEV" />\n <script src="https://yahoo.com"></script><title>Yahoo</title></head>\n<body>\n<a class="site-title" href="/">\n<a class="site-title" href="/test">\n</body>\n</html>';
    const result = linkProcessor.getAnswers(websiteData);
    describe('Website Title', () => {
        it('should return title of the website', () => {            
            expect(result.title).to.equal('Yahoo');
        });
    });
    describe('Number of All Clickable Links', () => {
        it('should return the number of website links', () => {
            expect(result.allLinks).to.equal(2);
        });
    });
    describe('Number of Unique Domains', () => {
        it('should return the number Of unique domains', () => {
            expect(result.uniqueDomains).to.equal(2);
        });
    });
    describe('Site Uses Google Anaytic', () => {
        it('should return false if site is yahoo', () => {
            expect(result.usesGA).to.equal(false);
        });
    });
    describe('Site Protocol', () => {
        it('should return true if the site uses https protocol', () => {
            result.siteProtocol = linkProcessor.getSiteProtocol(protocolData);
            expect(result.siteProtocol).to.equal(false);
        });
    });
});